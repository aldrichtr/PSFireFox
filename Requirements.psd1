@{
    PSDependOptions  = @{
        source = 'PSGalllery'
    }
    Plaster          = 'latest'
    InvokeBuild      = 'latest'
    Pester           = 'latest'
    PSScriptAnalyzer = 'latest'
    BuildHelpers     = 'latest'
}
