___This is a pre-release version so expect changes___
# PSFireFox


Manage FireFox bookmarks, tags and other profile data

## Overview

PSFireFox provides functions to interact with a FireFox installation.  It's primary purpose
is to provide a means of managing firefox bookmarks from scripts, modules and the command-line

### FireFox

If FireFox is running, you can get the Windows, Tabs, and other information about the application
using `Get-FireFox`

### Firefox bookmarks

FireFox provides several features that help with classifying, organizing and arranging bookmarks.
The most obvious is the folder system.  You can get a list of folders using `Get-FireFoxFolder`

### FireFox Profiles

FireFox can have several profiles, and they are stored in ini-style files in the users home directory.
Although mostly used by other functions in the module, you can use `Get-FireFoxProfile` to read them.

## For Developers

Although there are several 'PowerShell Module Development' formats, this module follows the following
conventions:

- Source files are in the './PSFireFox' folder.
- Source files are organized in folders according to their type and visibility ('Public', 'Private', 'Enum', etc.)
- Each function is in it's own .ps1 file, named `<Verb>-<Noun>.ps1`.  One function per file.
- All tests are in 'Tests' with the name `<thing to be tested>.Tests.ps1`
- Invoke-Build is used to test, stage, build, etc.
- `.build.ps1` loads all supporting tasks from ./Build/*.Tasks.ps1

To get up and running, clone the directory, run `Invoke-Build Configure`
That will set up any directories, as well as install Modules found in `Requirements.psd1`

After that, any changes can be tested with `Invoke-Build Test`

### References

https://github.com/mozilla/firefox-data-store-docs
