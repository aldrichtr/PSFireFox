
Function Get-FireFoxProfile {
    <#
    .SYNOPSIS
        Get the directory for the firefox profile
    .DESCRIPTION
        The directory firefox stores all the bookmarks, sessions, etc.
    #>
    [CmdletBinding()]
    param(
        # Profile name, return the Default if not specified
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [string]
        $Profile = "Default",

        # The install within the profile
        [Parameter(
            Mandatory = $false,
            ValueFromPipeline = $true
        )]
        [string]
        $Install = ""
    )

    $ff_directory = "$env:APPDATA\Mozilla\Firefox"
    $profile_ini = "$ff_directory\profiles.ini"
    $install_ini = "$ff_directory\installs.ini"

    $installs = Get-Content $install_ini | ConvertFrom-Ini
    $install_id = $installs.Keys[0] # this assumes that the first install is the desired install?
    Write-Verbose "Firefox install ID: $install_id"
    $profiles = Get-Content $profile_ini | ConvertFrom-Ini
    $profile_dir = $profiles["Install$install_id"].$Profile
    Write-Verbose "$Profile profile directory is $profile_dir in $profile_ini"

    if (Test-Path "$ff_directory\$profile_dir") {
        return "$ff_directory\$profile_dir"
    } else {
        throw "Profile lookup returned an invalid path`n$ff_directory\$profile_dir"
    }
}
