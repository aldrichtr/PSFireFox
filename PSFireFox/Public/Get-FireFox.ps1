

Function Get-FireFox {
    <#
    .SYNOPSIS
        List windows and tabs in a running instance of FireFox
    .DESCRIPTION
        FireFox maintains the 'session state' in a file in the profile directory.
        This file is a custom format, consisting of a json file,  with a custom
        header, and compressed using lz4 format.
        In order to retrieve the information in this file, we need a small utility
        called [dejsonlz4.exe](https://github.com/avih/dejsonlz4)
        The utility for Windows is included in the Module
    .EXAMPLE
        PS C:> $ff = Get-Firefox
        PS C:> foreach ($win in $ff.windows) {
                    Write-Output "Window $($win.zIndex)"
                    foreach ($tab in $win.tabs.entries) {
                        Write-Output "  Tab $($tab.ID) $($tab.title) $($tab.url)"
                    }
                }
    #>
    [CmdletBinding()]
    param()
    $profile_dir = Get-FireFoxProfile

    $session_recovery_file = "$profile_dir\sessionstore-backups\recovery.jsonlz4"

    if (Test-Path $session_recovery_file) {
        $session = dejsonlz4 $session_recovery_file | ConvertFrom-Json
    } else {
        throw "Session file not found, is firefox running?"
    }
    $session
}
