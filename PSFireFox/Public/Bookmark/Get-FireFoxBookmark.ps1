Function Get-FireFoxBookmark {
    <#
    .SYNOPSIS
        Lookup a bookmark in the firefox bookmark database
    .DESCRIPTION
        Firefox has a sophisticated schema for saving bookmarks.
        - moz_anno_attributes :: Annotation attributes
        - moz_annos :: Annotations
        - moz_bookmarks :: Bookmarks
        - type:
            - 1 :: Entry
            - 2 :: Folder
            - 3 :: Separator
        - moz_favicons :: Favorite icons > including URL
        - moz_historyvisits :: A history of the number of times a site has been visited
        - moz_inputhistory :: A history of URLs typed by the user
        - moz_items_annos :: Item annotations
        - moz_keywords :: keywords
        - moz_places :: Places/sites visited
    #>
    [CmdletBinding()]
    param(
        # Tag name of the bookmark
        [Parameter()]
        [string]$Tag,
        # Bookmark folder
        [Parameter()]
        [string]$Folder
    )

    $bm_query = "SELECT * FROM moz_bookmarks ORDER BY id"

    try {
        $profile_dir = Get-FireFoxProfile
        $bookmark_db = "$profile_dir\places.sqlite"
        Write-Verbose "Getting bookmarks from $bookmark_db"
        $db = New-SqliteConnection $bookmark_db -ReadOnly
        $records = Invoke-SqliteQuery -SQLiteConnection $db -Query $bm_query
    } catch {
        $err = $_
        throw "An error occured getting bookmarks`nID: $($err.FullyQualifiedErrorId)`tCategory $($err.CategoryInfo)`n$($err.ScriptStackTrace)"
    }

    $records
}
